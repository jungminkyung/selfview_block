

class BlockTest {
    constructor() {
        if( _game.canvasid === '' ) {
            console.error(' _game.canvasid error');
        }
        else {
            console.log(' %c_game.canvasid success','color:green');
        }
    }

    testgetStage() {
        _game.getStage();
        let logtext = 'testgetStage()';
        QUnit.test(logtext, function(assert){
            assert.ok( _game.point > 0 , "passed!");
        });
    }
    testarrayToSolution() {
        let goodseearray = [];
        goodseearray[0] =
            [0,0,1,1,
                0,1,0,0,
                0,1,0,0,
                0,0,0,0];
        goodseearray[1] =
            [0,0,0,0,
                0,0,1,0,
                0,0,0,0,
                0,0,0,0];

        let x;
        let solution = [];
        for(x=0; x<4; x++) {
            solution[x] = [];
            for(let z=0; z<4; z++) {
                solution[x][z] = [];
                for(let y=0; y<4; y++) {
                    solution[x][z][y] = 2;
                }
            }
        }
        solution[2][0][0] = 1;
        solution[3][0][0] = 1;
        solution[1][1][0] = 1;
        solution[1][2][0] = 1;
        solution[2][1][1] = 1;
        let solutionresult = _game.arrayToSolution(goodseearray);

        let logtext = 'testarrayToSolution()';
        QUnit.test(logtext, function(assert){
            assert.ok( JSON.stringify(solutionresult) === JSON.stringify(solution), "passed!");
        });
    }
    testdrawCoordinate() {
        for(let i=0; i< _game.scene.children.length; i++) {
            let children = _game.scene.children[i];
            if(children['type'] == 'Mesh') {
                let firstMeshName = _game.scene.children[i].material.name;
                let logtext = 'testdrawCoordinate()';
                QUnit.test(logtext, function(assert){
                    assert.ok( firstMeshName === 'x0y0z0', "passed!");
                });
                break;
            }
        }
        console.log(_game.scene,'scene');

    }
    testtimeDecreaseStart() {
        let logtext = 'testtimeDecreaseStart()';
        QUnit.test(logtext, function(assert){
            assert.ok( _game.remainTimer, "passed!");
        });

    }
    testtimeDecreaseTick() {

        let prevtime = _game.remaintime;
        _game.timeDecreaseTick();
        let nexttime = _game.remaintime;
        let logtext = 'testtimeDecreaseTick()';
        QUnit.test(logtext, function(assert){
            assert.ok( prevtime-1 === nexttime, "passed!");
        });

    }
    testmakeFabricCanvas() {
        let logtext = 'testmakeFabricCanvas()';
        QUnit.test(logtext, function(assert){
            assert.ok( _game.canvas_fabric, "passed!");
        });

    }
    testskip() {
        let prevskipchange = _game.skipChance;
        _game.skip();
        let currentskipchange = _game.skipChance;
        let logtext = 'testskip()';
        QUnit.test(logtext, function(assert){
            assert.ok( prevskipchange-1 === currentskipchange, "passed!");
        });

    }
    testcomplete() {
        //정답생성, 넘기기, 넘겨진것확인
        //오답생성, 넘기기, 안념겨진것 확인
        //clear
        let prevlevel = _game.level;
        let prevmyquestioncount = _game.myquestioncount;
        _game.forceSolutionCreate();
        _game.complete();
        let nextlevel = _game.level;
        let nextmyquestioncount = _game.myquestioncount;

        let logtext = 'testcomplete()';
        QUnit.test(logtext, function(assert){
            assert.ok( prevlevel !== nextlevel || prevmyquestioncount !== nextmyquestioncount, "passed!");
        });

    }
    testtransferSolution() {
        let goodseearray = [];
        goodseearray[0] =
            [0,0,1,1,
                0,1,0,0,
                0,1,0,0,
                0,0,0,0];
        goodseearray[1] =
            [0,0,0,0,
                0,0,1,0,
                0,0,0,0,
                0,0,0,0];

        let x;
        let solution = [];
        for(x=0; x<4; x++) {
            solution[x] = [];
            for(let z=0; z<4; z++) {
                solution[x][z] = [];
                for(let y=0; y<4; y++) {
                    solution[x][z][y] = 2;
                }
            }
        }
        solution[2][0][0] = 1;
        solution[3][0][0] = 1;
        solution[1][1][0] = 1;
        solution[1][2][0] = 1;
        solution[2][1][1] = 1;

        let solution90 = [];
        for(x=0; x<4; x++) {
            solution90[x] = [];
            for(let z=0; z<4; z++) {
                solution90[x][z] = [];
                for(let y=0; y<4; y++) {
                    solution90[x][z][y] = 2;
                }
            }
        }

        let solution180 = [];
        for(x=0; x<4; x++) {
            solution180[x] = [];
            for(let z=0; z<4; z++) {
                solution180[x][z] = [];
                for(let y=0; y<4; y++) {
                    solution180[x][z][y] = 2;
                }
            }
        }

        let solution270 = [];
        for(x=0; x<4; x++) {
            solution270[x] = [];
            for(let z=0; z<4; z++) {
                solution270[x][z] = [];
                for(let y=0; y<4; y++) {
                    solution270[x][z][y] = 2;
                }
            }
        }
        solution90[1][1][0] = 1;
        solution90[2][1][0] = 1;
        solution90[3][2][0] = 1;
        solution90[3][3][0] = 1;
        solution90[2][2][1] = 1;

        solution180[2][1][0] = 1;
        solution180[2][2][0] = 1;
        solution180[0][3][0] = 1;
        solution180[1][3][0] = 1;
        solution180[1][2][1] = 1;

        solution270[0][0][0] = 1;
        solution270[0][1][0] = 1;
        solution270[1][2][0] = 1;
        solution270[2][2][0] = 1;
        solution270[1][1][1] = 1;

        let logtext;
        logtext = 'testtransferSolution(90)';
        QUnit.test(logtext, function(assert){
            assert.ok( JSON.stringify(_game.transferSolution(solution,90)) === JSON.stringify(solution90), "passed!");
        });


        logtext = 'testtransferSolution(180)';

        QUnit.test(logtext, function(assert){
            assert.ok( JSON.stringify(_game.transferSolution(solution,180)) === JSON.stringify(solution180) , "passed!");
        });


        logtext = 'testtransferSolution(270)';
        QUnit.test(logtext, function(assert){
            assert.ok( JSON.stringify(_game.transferSolution(solution,270)) === JSON.stringify(solution270)  , "passed!");
        });

    }
    testforceSolutionCreate() {
        _game.forceSolutionCreate();
        let xyzList = _game.getXYZArrayFromAddedBoxObjectArray();
        let solution = _game.xyzArrayToSolutionArray(xyzList);
        let logtext;
        logtext = 'testforceSolutionCreate() getXYZArrayFromAddedBoxObjectArray() xyzArrayToSolutionArray()';
        QUnit.test(logtext, function(assert){
            assert.ok( JSON.stringify(solution) === JSON.stringify(_game.solution)  , "passed!");
        });


        logtext = 'testisCollectAnswer()';
        QUnit.test(logtext, function(assert){
            assert.ok(  _game.isCollectAnswer(solution)   , "passed!");
        });

    }
    testplaceBlock() {
        _game.clear();
        let prevlength = _game.addedBoxObjectArray.length;
        let cube = _game.placeBlock('x1y1z1');
        let nextlength = _game.addedBoxObjectArray.length;
        let logtext;
        logtext = 'testplaceBlock()';
        QUnit.test(logtext, function(assert){
            assert.ok(   prevlength !== nextlength  , "passed!");
        });


        _game.placeBlockSave = cube;
        _game.placeBlockLength = 1;
        _game.removeBlock();
        logtext = 'removeBlock()';
        QUnit.test(logtext, function(assert){
            assert.ok(   _game.addedBoxObjectArray.length === 0  , "passed!");
        });

        _game.placeBlockSave = '';
        _game.placeBlockLength = 0;

    }
    testisCoordinate() {
        let logtext;
        logtext = 'testisCoordinate() x0y0z0';
        QUnit.test(logtext, function(assert){
            assert.ok(   _game.isCoordinate('x0y0z0') === true  , "passed!");
        });

        logtext = 'testisCoordinate() z1y1z1';
        QUnit.test(logtext, function(assert){
            assert.ok(  _game.isCoordinate('x1y1z1') === false   , "passed!");
        });

    }
    testaddBoxByName() {
        _game.clear();
        let logtext;
        logtext = 'testaddBoxByName() ';
        let cube = _game.addBoxByName('x2y1z1');
        QUnit.test(logtext, function(assert){
            assert.ok(   _game.isBoxAdded  , "passed!");
        });

        logtext = 'cube added';
        QUnit.test(logtext, function(assert){
            assert.ok(   cube  , "passed!");
        });
    }
    testgetXYZByBoxname() {
        let logtext = 'getXYZByBoxname()';
        let a = _game.getXYZByBoxname('x1y1z1');
        let b = { x:"1",y:"1",z:"1"};
        QUnit.test(logtext, function(assert){
            assert.ok(  JSON.stringify(a) === JSON.stringify(b)   , "passed!");
        });

    }




}
