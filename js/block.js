var _block = '';
var _game = '';
class Block {
    constructor(canvasid,userseq) {
        this.canvasid = canvasid;
        _block = _game = this;
        if(canvasid === '' || canvasid === undefined) {
            console.log("no canvasid 생성자 오류");
        }
        this.userseq = userseq;
        this.width = 320;
        this.height = 480;
        this.pickPosition = {};
        this.prevAddBoxName = '';
        this.maxtime = 60*3;
        this.skipChance = 2;
        this.currentskip = 0;
        this.level = 1;
        this.lengthweight = 1; //길이 가중치. web일때는 곱해서 늘어난다.
        this.widthweight = 1;
        this.heightweight = 1;
        this.placeBlockSave = '';
        this.placeBlockLength = '';
        this.mouse = {
            x: 0,
            y: 0,
        };
        this.mypoint = 0; //누적 내점수
        this.point = 0; //문제당 점수
        this.questioncount = 0; //스테이지당 풀어야 하는 문제
        this.myquestioncount = 0; //스테이지당 내가 푼 문제
        this.viewedQuestionArray = []; //출제된 문제 저장
        this.addedBoxObjectArray = []; //추가된 박스 오브젝트. 박스 자동 제거시 사용
        this.remaintime = this.maxtime;
        this.samecount = 0; //중복스테이지 카운트
        this.soundPreload();
        this.browser = 'mobile';
        this.silence = false;
        this.currentSkipCount = 0;
        this.pointerdown = 2;
        this.pointerup = 2;

        this.camera_position_x = 7.215;
        this.camera_position_z = 7.138;
        this.camera_position_y = -1.087;

        this.nopickingColor = true; // 마우스피킹 컬러 사용안함.

        this.debug = false;
        this.yoffset = -4;
        if(this.debug) {
            this.nocontrol = false;    //orbitcontrol 을 사용함
        }
        else {
            this.nocontrol = true;    //orbitcontrol 을 사용안함
        }

    }
    /*
    * 초기화
    */
    init() {
        console.log('=============================init============================');
        /*if(!this.canvas) {
            this.canvas = new fabric.Canvas( this.canvasid, {  width: this.width ,height:this.height } );
        }*/
        if(!this.canvas) {
            this.canvas = document.querySelector('#'+this.canvasid);
        }
        if(!this.renderer) {
            let canvas = this.canvas;
            this.renderer = new THREE.WebGLRenderer({canvas, alpha:true});
        }

        if(!this.camera) {
            this.fov = 85;
            let aspect;
            aspect = this.width / this.height;
            const near = 0.1;
            const far = 100;
            this.camera = new THREE.PerspectiveCamera(this.fov, aspect, near, far);
            this.camera.position.x = this.camera_position_x;
            this.camera.position.z = this.camera_position_z;
            this.camera.position.y = this.camera_position_y;
            this.camera.lookAt(0,-1.5,0); //orbitcontrol이 있으면 orbitcontrol의 target.set에 의해 무시됨. 컨트롤이 없을때, 이 값으로 설정된다.
        }

        //_game.cameraHelper = new THREE.CameraHelper(this.camera);

        if(!this.scene) {
            this.scene = new THREE.Scene();
            //this.scene.add(_game.cameraHelper);
        }
        if(!this.control && !this.nocontrol) {
            this.controls = new THREE.OrbitControls(this.camera, this.canvas);
            this.controls.target.set(0, -1, 0);
            this.controls.update();
        }
        if(!this.light1) {
            const color = 0xFFFFFF;
            const intensity = 1.5;
            this.light1 = new THREE.DirectionalLight(color, 0.9);
            this.light1.position.set(-3.3,  -2.7,  3.6);
            this.light1.target.position.set(0,  -1,  5);
            this.scene.add(this.light1);

            //let helper1 = new THREE.DirectionalLightHelper(this.light1);
            //this.scene.add(helper1);

            this.light2 = new THREE.DirectionalLight(color, 2.5);
            this.light2.position.set(10,  5,  -5.7);
            this.light2.target.position.set(0,  1,  5);
            this.scene.add(this.light2);

            //let helper2 = new THREE.DirectionalLightHelper(this.light2);
            //this.scene.add(helper2);

            /*this.light = new THREE.AmbientLight(0xFFFFFF, 1);
            this.scene.add(this.light);*/
        }
        this.prevAddBoxName = '';
        this.solution = '';
        this.goodseearray = '';
        this.addedBoxObjectArray = []; //추가된 박스 오브젝트. 박스 자동 제거시 사용
        this.isBoxAdded = false;
    }

    blockGame() {
        this.adjustCanvasSize();
        this.init();
        this.typeselectScreen('web');
        this.mode='Block';
        this.getStage();
        this.drawStartScreen(); //초기화면을 그린다.

        _game.timeDecreaseStart();

        /*$.when( _game.log2('start') ).then(
            function (logseq) {
                _game.logseq = logseq;
        });;;*/

    }
    /*
    * window크기에 따라 weight 조정
    */
    lengthweightAutoSetting() {
        var windowwidth = $(window).outerWidth();
        //600일때 1.875   = currentwidth : x
        if(windowwidth < 600) {
            //console.log('weight 조정:'+(windowwidth*1.875)/600);
            this.lengthweight = (windowwidth*1.875)/600;
            this.widthweight = (windowwidth*1.875)/600;
            this.heightweight = (windowwidth*1.875)/600;

        }
    }
    //종류선택화면을 만들어 뿌린다. Relaxed(position) Silient(position,color) Classic(position,sound) Advanced(color,position,sound)
    typeselectScreen(browser) {
        this.browser = browser;
        if(this.browser === 'web') {
            this.lengthweight = 1.505;
            this.widthweight = 1.505;
            this.heightweight = 1.505;
        } else {
            this.lengthweight = 1;
            this.widthweight = 1;
            this.heightweight = 1;
        }
        this.lengthweightAutoSetting();
    }
    getStage() {
        _game.point = 0;
        switch(_game.level) {
            case 1:
                _game.blockcount = 3;
                _game.point = 5;
                _game.questioncount = 1;
                break;
            case 2:
                _game.blockcount = 4;
                _game.point = 5;
                _game.questioncount = 2;
                break;
            case 3:
                _game.blockcount = 5;
                _game.point = 10;
                _game.questioncount = 2;
                break;
            case 4:
                _game.blockcount = 6;
                _game.point = 15;
                _game.questioncount = 2;
                break;
            case 5:
                _game.blockcount = 7;
                _game.point = 20;
                _game.questioncount = 2;
                break;
            case 6:
                _game.blockcount = 8;
                _game.point = 25;
                _game.questioncount = 2;
                break;
            case 7:
                _game.blockcount = 9;
                _game.point = 30;
                break;
            case 8:
                _game.blockcount = 10;
                _game.point = 35;
                break;
            default:
                _game.log2('end')
                _game.gameover();
                return;

        }

        _game.makeSolution();
    }

    //보기좋은 배열형식으로 된 값을 정답배열로 바꾼다.
    arrayToSolution(array) {
        if(array === undefined) {
            alert('오류발생(22)');
            return;
        }

        let x,y,z;
        let solution = [];
        let nomatch = 1;
        for(x=0; x<4; x++) {
            solution[x] = [];
            for(let z=0; z<4; z++) {
                solution[x][z] = [];
                for(let y=0; y<4; y++) {
                    solution[x][z][y] = 2;
                }
            }
        }

        for(let i=0; i< array.length; i++) {
            y = i;
            for(let j=0; j< array[i].length; j++) {
                if( array[i][j] === 1) {
                    x = j % 4;
                    z = Math.floor(j / 4);
                    solution[x][z][y] = 1;
                    nomatch = 2;
                    //console.log('x'+x+' z'+z+' y'+y);
                }
            }
        }
        if(nomatch === 1) {
            alert('no match');
        }
        return solution;
    }
    /*
    *  화면 그리기
    */
    drawStartScreen() {

        this.drawCoordinate(); //기본평면
        this.addPicking();    //마우스 피킹
        this.addEraseBtn();   //블록삭제기능버튼

        this.drawFabricjs();
        this.addActionButtons(); //넘어가기,초기화,완료버튼 추가

        if(this.debug) {
            this.addCameraGui(); //GUI 임시추가
        }
    }

    drawCoordinate() {

        const scene = this.scene;

        let boxWidth = 1;
        let boxHeight = 0.1;
        let boxDepth = 1;
        const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);
        const loader = new THREE.TextureLoader();

        let coordnateIds = [];

        for(let x=0; x<=3; x++) {
            coordnateIds[x] = [];
            for(let z=0; z<=3; z++) {
                coordnateIds[x][z] = 'x'+x+'y0z'+z;
                let material = new THREE.MeshPhongMaterial({
                    map: loader.load('./images/floor.png'),
                    name: coordnateIds[x][z],
                });
                let cube = new THREE.Mesh(geometry, material);
                cube.position.x = x;
                cube.position.z = z;
                cube.position.y = -0.5 + _game.yoffset;
                scene.add(cube);
            }
        }

        const arrowGeometry = new THREE.PlaneGeometry(0.5,0.5);
        let arrowMaterial = new THREE.MeshPhongMaterial({
            map: loader.load('./images/positionarrow.png'),
            name: 'arrow',
            transparent: true,
            alphaTest: 0.1,
            color: 'black',
            side: THREE.DoubleSide,
        });
        let arrowcube = new THREE.Mesh(arrowGeometry, arrowMaterial);
        _game.arrowcube = arrowcube;
        arrowcube.position.x = -0.1;
        arrowcube.position.z = 3.4;
        arrowcube.position.y = -5.49 + _game.nodebugoffset;


        _game.arrow = arrowcube;
        _game.arrow.rotation.x = 0.0;
        _game.arrow.rotation.y = 0.94;
        _game.arrow.rotation.z = -6;
        scene.add(arrowcube);

        requestAnimationFrame(this.render);
    }

    drawFabricjs() {
        if(_game.isGameOver === 1) {
            return;
        }
        this.makeFabricCanvas();
        this.drawFloor(); //윗면도
        this.drawFront(); //정면도
        this.drawRightSide(); //우측면도
        this.drawText();
        this.drawJumsu();
        this.drawMinBlockText();
        this.drawTime();

    }
    timeDecreaseStart() {
        if(_game.remainTimer) {
            clearInterval(_game.remainTimer);
            console.log('remaintimer clear timeDecreaseStart');
            _game.remainTimer = null;
        }
        //console.log('remainTimer set');
        this.remainTimer = setInterval( this.timeDecreaseTick , 1000 );
    }
    timeDecreaseTick() {
        //1초씩 감소
        _game.remaintime--;
        if(_game.remaintime < 0) {
            //00:00 종료
            //console.log('타이머 종료');
            clearInterval(_game.remainTimer);
            _game.remainTimer = null;
            //_game.gameover();

            $.when( _game.log2('end') ).then(

                function () {
                    _game.gameover();
                }
            );

            return;
        }
        _game.remainTimeToTextDraw();
        //위치결정한뒤 보여줌 or 없앰.
        _game.canvas_fabric.renderAll();
    }
    makeFabricCanvas() {
        if(_game.canvas_fabric === '' || _game.canvas_fabric === undefined) {
            if(_game.browser === 'web') {
                _game.canvas_fabric = new fabric.Canvas( 'cmaps', {  } );
            }
            else if(this.browser === 'mobile') {
                _game.canvas_fabric = new fabric.Canvas( 'cmaps', {  });
            }
            else if(this.browser === 'ipad') {
                _game.canvas_fabric = new fabric.Canvas( 'cmaps', {  } );
            }
        }
        else {
            _game.canvas_fabric.clear();
        }


    }
    drawText() {
        let toptext;
        toptext = '제시된 투영도에 맞도록 블록을 쌓으세요.';
        let toptextfabric = _game.textadd(toptext, {
            fill:'#FFFFFF', fontSize:15 * _game.lengthweight , fontWeight:'bold', top:18* _game.lengthweight, fontFamily: 'Comic Sans',
            textAlign:'center'
        } , {align:'xcenter'});
        _game.canvas_fabric.add(toptextfabric);
    }
    drawMinBlockText() {
        let minblocktext = _game.textadd('최소블록', {
            fill:'#FFFFFF', fontSize:11 * _game.lengthweight,  fontFamily: 'Comic Sans', left: 120 * _game.widthweight, top:59 * _game.heightweight
        } , { });
        _game.canvas_fabric.add(minblocktext);
        let blockcounttext = _game.blockcount+'';
        let minblocknumber = _game.textadd(blockcounttext+'개 ', {
            fill:'#FFFFFF', fontSize:11 * _game.lengthweight,  fontFamily: 'Comic Sans', left: 175 * _game.widthweight, top:59 * _game.heightweight
        } , { });
        _game.canvas_fabric.add(minblocknumber);
        _game.minblocknumber = minblocknumber;

    }
    drawJumsu() {
        let scoretext = _game.textadd('점수 ', {
            fill:'#FFFFFF', fontSize:11 * _game.lengthweight,  fontFamily: 'Comic Sans', left: 37 * _game.widthweight, top:59 * _game.heightweight
        } , { });
        _game.canvas_fabric.add(scoretext)

        let point = _game.mypoint + '';
        let scoreFabric = _game.textadd(point, {
            fill:'#FFFFFF', fontSize:11 * _game.lengthweight,  fontFamily: 'Comic Sans', left: 70 * _game.widthweight, top:59 * _game.heightweight
        } , { });
        _game.canvas_fabric.add(scoreFabric);
    }
    drawTime() {
        //남은시간 글자
        let remaintimetexttext = _game.textadd('남은 시간', {
            fill:'#FFFFFF', fontSize:11 * _game.lengthweight,  fontFamily: 'Comic Sans', left: 214 * _game.widthweight, top:59 * _game.heightweight
        }, {} );
        _game.canvas_fabric.add(remaintimetexttext);

        _game.remaintimetext = _game.textadd('00', {
            fill:'#FFFFFF', fontSize:11 * _game.lengthweight,  fontFamily: 'Comic Sans', left: 264 * _game.widthweight, top:59 * _game.heightweight
        }, {} );
        _game.canvas_fabric.add(_game.remaintimetext);
        _game.remainTimeToTextDraw();
    }
    remainTimeToTextDraw() {
        var minute = Math.floor(this.remaintime / 60);
        var second = this.remaintime % 60;
        var minutetext = minute+ '';
        var secondtext = second+'';
        if(secondtext.length === 1) {
            secondtext = '0'+secondtext;
        }
        this.remaintimetext.text = ' '+minutetext+':'+secondtext;
    }
    drawMap(placetext,maparray) {
        let canvas;
        let mapname;
        let startx = 0,starty = 0;
        canvas = _game.canvas_fabric;

        let x,y;
        let left = 0,top = 0;
        let fill;
        let boxwidth = 20;
        //남은칸을 적절히 배분
        let other = (_game.width - (boxwidth*4)*3 - 10*2 - 20) /4;
        if(placetext === 'floor') {
            //canvas = new fabric.Canvas( 'cfloor', {  });
            mapname = '평면도';
            startx = 10 + other;
            starty = 80 * _game.lengthweight;
        }
        else if(placetext === 'front') {
            //canvas = new fabric.Canvas( 'cfront', {  });
            mapname = '정면도';
            startx = 100 + other + other;
            starty = 80 * _game.lengthweight;
        }
        else if(placetext === 'rightside') {
            //canvas = new fabric.Canvas( 'crightside', {  });
            mapname = '우측면도';
            startx = 190 + other + other + other;
            starty = 80 * _game.lengthweight;
        }

        for(x=0 ; x<4 ; x++) {
            left = x * boxwidth + startx;
            for(y=0; y<4; y++) {
                top = y * boxwidth + starty;
                fill = '#FFFFFF';
                if(maparray[x][y] === 1) {
                    fill = '#67CBDB';
                }

                let rect = new fabric.Rect({
                    top: top,
                    left: left,
                    width: boxwidth,
                    height: boxwidth,
                    fill: fill,
                    stroke: 'black',
                    strokeWidth: 1
                });
                _game.setLock(rect);
                canvas.add(rect);

            }
        }
        let text = new fabric.Text(mapname, {fill:'#FFFFFF', fontSize:14,  fontFamily: 'Comic Sans', left: 20+startx, top:84+starty});
        _game.setLock(text);
        canvas.add(text);

    }
    setLock( target ) {
        target.hasBorders = false;
        target.hasControls = true;
        target.lockRotation = true;
        target.lockMovementX = true;
        target.lockMovementY = true;
        target.selection = false;
        target.selectable = false;
        return target;
    }

    drawFloor() {
        //평면도
        let map = _game.getFloorMap();
        //console.log(map,'평면도');
        _game.drawMap('floor',map);
    }
    getFloorMap() {
        //모든 xz평면을 계산해서 있는곳에 체크
        let x,y,z;
        let maparray = [];
        for(x=0 ; x < 4; x++) {
            maparray[x] = [];
            for(y=0; y < 4; y++) {
                maparray[x][y] = 2;
            }
        }
        for(y=0 ; y<4 ; y++) {
            for (x = 0; x < 4; x++) {
                for (z = 0; z < 4; z++) {
                    if( _game.solution[x][z][y] === 1) {
                        maparray[x][z] = 1;
                    }
                }
            }
        }
        return maparray;
    }
    drawFront() {
        //정면도
        let map = _game.getFrontMap();
        //console.log(map,'정면도');
        _game.drawMap('front',map);
    }
    getFrontMap() {
        //z제외하고 계산
        let x,y,z;
        let maparray = [];
        for(x=0 ; x < 4; x++) {
            maparray[x] = [];
            for(y=0; y < 4; y++) {
                maparray[x][y] = 2;
            }
        }

        for(z=0 ; z<4 ; z++) {
            for (x = 0; x < 4; x++) {
                for (y = 0; y < 4; y++) {
                    if( _game.solution[x][z][y] === 1) {
                        maparray[x][3-y] = 1; // y=0일때 y=3에표시 y=1일때 y=2에 표시 (배열(좌상이 원점)을 데카르트 좌표계(좌하가원점) 로변환)
                    }
                }
            }
        }
        return maparray;
    }
    drawRightSide() {
        //우측면도
        let map = _game.getRightSideMap();
        //console.log(map,'우측면도');
        _game.drawMap('rightside',map);
    }
    getRightSideMap() {
        //x제외하고 계산
        let x,y,z;
        let maparray = [];
        for(x=0 ; x < 4; x++) {
            maparray[x] = [];
            for(y=0; y < 4; y++) {
                maparray[x][y] = 2;
            }
        }
        for(x=0 ; x<4 ; x++) {
            for (y = 0; y < 4; y++) {
                for (z = 0; z < 4; z++) {
                    if( _game.solution[x][z][y] === 1) {
                        maparray[3-z][3-y] = 1; //z=0 -> 3에표시
                    }
                }
            }
        }
        return maparray;
    }
    addActionButtons() {
        this.addSkipButton();
        this.addClearButton();
        this.addCompleteButton();
    }
    addSkipButton() {
        //넘어가기 버튼
        let width = _game.width / 4;
        let height = 30 * _game.lengthweight;
        let $skipbtn = $('#skipbtn');
        $skipbtn.css('width',width+'px').css('height', height+'px');
        let other = ((_game.width - width*3)-20)/3;
        let bottom = 20 * _game.lengthweight;
        $skipbtn.css('left',other).css('bottom',bottom);
    }
    addClearButton() {
        //블럭 초기화버튼
        let width = _game.width / 4;
        let height = 30 * _game.lengthweight;
        let $clearbtn = $('#clearbtn');
        $clearbtn.css('width',width+'px').css('height', height+'px');
        let other = ((_game.width - width*3)-20)/3
        let bottom = 20 * _game.lengthweight;
        $clearbtn.css('left',other+width+other).css('bottom',bottom);
    }
    addCompleteButton() {
        //완료버튼
        let width = _game.width / 4;
        let height = 30 * _game.lengthweight;
        let $completebtn = $('#completebtn');
        $completebtn.css('width',width+'px').css('height', height+'px');
        let other = ((_game.width - width*3)-20)/3;
        let bottom = 20 * _game.lengthweight;
        $completebtn.css('left',other+width+other+width+other).css('bottom',bottom);
    }
    skip() {
        //console.log('넘어가기skip');
        if(_game.skipChance <= 0) {
            console.log('스킵횟수 초과');
            return;
        }
        _game.currentSkipCount++;
        _game.skipChance--;
        _game.complete('skip');
        _game.skipButtonUpdate();
    }
    skipButtonUpdate() {
        $('#skipbtn').val('(넘어가기('+ _game.currentSkipCount +'/2)');
    }

    clear() {
        //console.log('추가된도형 초기화 버튼');
        for(let i=0 ; i < _game.addedBoxObjectArray.length; i++) {
            _game.scene.remove( _game.addedBoxObjectArray[i] );
        }
        _game.addedBoxObjectArray = [];

        _game.camera.position.x = _game.camera_position_x;
        _game.camera.position.z = _game.camera_position_z;
        _game.camera.position.y = _game.camera_position_y;
        if(!_game.nocontrol) {
            _game.controls.target.set(0, -1, 0);
            _game.controls.update();
        }

        _game.prevAddBoxName = '';
        _game.isBoxAdded = false;
    }
    addedBoxObjectArray_del(target) {
        //console.log(target,'deltarget');

        let addedBoxObjectArray_new = [];
        for(let i=0; i< _game.addedBoxObjectArray.length; i++) {
            if( JSON.stringify( _game.addedBoxObjectArray[i] ) === JSON.stringify( target ) ) {
                continue;
            }
            addedBoxObjectArray_new.push( _game.addedBoxObjectArray[i] );
        }
        _game.addedBoxObjectArray = addedBoxObjectArray_new;
    }
    nextQuestion() {
        //화면초기화, 다음문제 셋팅 TODO
        _game.clear();
        _game.init();
        _game.getStage();
        _game.drawStartScreen();

    }
    complete(skip) {
        //console.log('완료버튼');
        //정답확인
        if ( _game.isScoring() ) {
            _game.mypoint += _game.point;
            _game.playSoundCorrect();
            _game.viewCollectImage();

        }
        else {
            _game.playSoundInCorrect();
            _game.viewInCollectImage();
            if(!skip) {
                return;
            }

        }
        //문제수 확인
        _game.myquestioncount++;
        if(_game.myquestioncount >= _game.questioncount) {
            _game.level++;
            _game.myquestioncount = 0;
        }
        _game.nextQuestion();
    }
    isScoring() {
        //정답인지 확인
        let xyzArray = _game.getXYZArrayFromAddedBoxObjectArray();
        let solutionArray = _game.xyzArrayToSolutionArray(xyzArray);
        //console.log(solutionArray,'solutionArray');

        let isCollectAnswer = false;
        if(_game.isCollectAnswer(solutionArray)) {
            isCollectAnswer = true;
        }
        if(!isCollectAnswer) {
            let solutionArray90 = _game.transferSolution(solutionArray,90) ;
            if(_game.isCollectAnswer(solutionArray90)) {
                isCollectAnswer = true;
            }
        }
        if(!isCollectAnswer) {
            let solutionArray180 = _game.transferSolution(solutionArray,180) ;
            if(_game.isCollectAnswer(solutionArray180)) {
                isCollectAnswer = true;
            }
        }
        if(!isCollectAnswer) {
            let solutionArray270 = _game.transferSolution(solutionArray,270) ;
            if(_game.isCollectAnswer(solutionArray270)) {
                isCollectAnswer = true;
            }
        }
        return isCollectAnswer;

    }
    //정답 회전 90,270 TODO
    transferSolution(solutionArray,degree) {
        let x,y,z;
        let solutionArray_new = [];
        for(x=0; x<4 ; x++) {
            solutionArray_new[x] = [];
            for(z=0 ; z<4 ; z++) {
                solutionArray_new[x][z] = [];
                for(y=0 ; y<4 ; y++) {
                    solutionArray_new[x][z][y] = 2;
                }
            }
        }
        //console.log(solutionArray,'solutionArray');
        //console.log(solutionArray_new,'solutionArray_new');

        if(degree === 90) {

            for(let i=0 ; i<4 ; i++ ) {
                for(let j=0; j<4; j++) {
                    for(let k=0; k<4; k++) {
                        if( solutionArray[i][j][k] === 1 ) {
                            solutionArray_new[3-j][i][k] = solutionArray[i][j][k];
                        }
                    }
                }
            }
        }
        else if(degree === 180) {

            for(let i=0 ; i<4 ; i++ ) {
                for(let j=0; j<4; j++) {
                    for(let k=0; k<4; k++) {
                        if( solutionArray[i][j][k] === 1 ) {
                            solutionArray_new[3-i][3 - j][k] = solutionArray[i][j][k];
                        }
                    }
                }
            }
        }
        else if(degree === 270) {

            for(let i=0 ; i<4 ; i++ ) {
                for(let j=0; j<4; j++) {
                    for(let k=0; k<4; k++) {
                        if( solutionArray[i][j][k] === 1 ) {
                            solutionArray_new[j][3-i][k] = solutionArray[i][j][k];
                        }
                    }
                }
            }
        }
        return solutionArray_new;

    }
    isCollectAnswer(solutionArray) {
        return JSON.stringify(_game.solution) === JSON.stringify(solutionArray);

    }
    //추가된 도형에서 xyz좌표의 리스트를 추출
    getXYZArrayFromAddedBoxObjectArray() {
        let name;
        let xyzList = [];
        let tempxyz;
        for(let i=0; i< _game.addedBoxObjectArray.length; i++) {
            name = _game.addedBoxObjectArray[i].material.name;
            tempxyz = _game.getXYZByBoxname(name);
            tempxyz['y']--;
            tempxyz['y'] = tempxyz['y'] + '';
            xyzList.push(tempxyz);
        }
        console.log(xyzList,'xyzList');
        return xyzList;
    }
    //xyz좌표 리스트를 정답형태로 추출
    xyzArrayToSolutionArray(xyzArray) {
        let x,y,z;
        let solution = [];
        for(x=0; x<4; x++) {
            solution[x] = [];
            for(z=0; z<4; z++) {
                solution[x][z] = [];
                for(y=0; y<4; y++) {
                    solution[x][z][y] = 2;
                }
            }
        }

        for(let i=0; i<xyzArray.length; i++) {
            solution[ xyzArray[i]['x'] ][ xyzArray[i]['z'] ][ xyzArray[i]['y'] ] = 1;
        }
        return solution;
    }
    addEraseBtn() {

    }
    addPicking() {
        class PickHelper {
            constructor() {
                this.raycaster = new THREE.Raycaster();
                this.pickedObject = null;
                this.pickedObjectSavedColor = 0;
            }
            pick(normalizedPosition, scene, camera, time) {
                // 이미 다른 물체를 피킹했다면 색을 복원합니다

                if (this.pickedObject) {
                    if(!_game.nopickingColor) {

                        this.pickedObject.material.emissive.setHex(this.pickedObjectSavedColor);
                    }
                    this.pickedObject = undefined;
                }
                if(_game.isBoxAdded === true) {
                    //console.log('already added');
                    return;
                }
                // 절두체 안에 광선을 쏩니다
                this.raycaster.setFromCamera(normalizedPosition, camera);
                // 광선과 교차하는 물체들을 배열로 만듭니다
                const intersectedObjects = this.raycaster.intersectObjects(scene.children);
                if (intersectedObjects.length) {

                    // 첫 번째 물체가 제일 가까우므로 해당 물체를 고릅니다
                    this.pickedObject = intersectedObjects[0].object;
                    // 기존 색을 저장해둡니다. emissive 색을 빨강/노랑으로 빛나게 만듭니다
                    if(!_game.nopickingColor) {
                        this.pickedObjectSavedColor = this.pickedObject.material.emissive.getHex();
                        this.pickedObject.material.emissive.setHex(0xFFFF00);
                    }

                    _game.placeBlockSave = this.pickedObject;
                    _game.placeBlockLength = intersectedObjects.length;
                }
                else {
                }

            }
        }

        _game.pickPosition = { x: 0, y: 0};
        _game.clearPickPosition();
        _game.pickHelper = new PickHelper();


        _game.canvas.addEventListener('mousemove', _game.setPickPosition);
        _game.canvas.addEventListener('mouseout', _game.clearPickPosition);
        _game.canvas.addEventListener('mouseleave', _game.clearPickPosition);
        _game.canvas.addEventListener('pointerdown', (event) => {
            event.preventDefault();
            console.log('pointerdown==================================================');
            _game.pointerdown = 1;
            _game.freezeDown = 2;
            _game.recordStartPosition(event);
            _game.freezeDownCheck();
            _game.canvas.addEventListener('pointermove', _game.recordMovement);
            _game.canvas.addEventListener('pointerup', function() {
                _game.pointerdown = 2;
                if(_game.freezeDown === 1) {
                }
                else {
                    _game.placeNoMovement();
                }
            });
        }, {passive: false});
        _game.canvas.addEventListener('touchstart', (event) => {
            event.preventDefault();
            _game.pointerdown = 1;
            _game.freezeDown = 2;
            _game.freezeDownCheck();
            _game.setPickPosition(event.touches[0]);
        }, {passive: false});
        _game.canvas.addEventListener('touchmove', (event) => {
            _game.setPickPosition(event.touches[0]);
        });
        _game.canvas.addEventListener('touchend', function() {
            _game.pointerdown = 2;
            if(_game.freezeDown === 1) {

            } else {
                _game.clearPickPosition();
            }
        } );
    }
    freezeDownCheck() {
        _game.pointerDownTime = new Date().getTime();
        //0.5초동안 누르고있으면 삭제한다.
        if(_game.freezeDownTimer) {
            clearTimeout(_game.freezeDownTimer);
            _game.freezeDownTimer = null;
        }
        _game.freezeDownTimer = setTimeout( () => {
            //도형을 누르고 움직이는것은 삭제하지않음. 도형을 누르고 가만히있으면 삭제함.
            if( _game.mouse.moveX < 5 && _game.mouse.moveY < 5) {
                if (_game.pointerdown === 1) {
                    _game.freezeDown = 1;
                    _game.removeBlock();
                    _game.prevAddBoxName = '';
                }
            }
        }, 500)
    }
    forceSolutionCreate() {
        _game.clear();
        for(let x=0 ; x<4; x++) {
            for(let z=0 ; z<4; z++) {
                for(let y=0 ; y<4; y++) {
                    if( _game.solution[x][z][y] === 1) {
                        _game.placeBlock('x'+x+'y'+y+'z'+z);
                    }
                }
            }
        }
    }
    placeBlock(name) {
        // 블록 쌓기
        //console.log(_game.placeBlockSave.material.name,' 교차한 오브젝트 이름 ');
        let cube;
        if(!name) {
            if(_game.placeBlockSave.material) {
                cube = _game.addBoxByName(_game.placeBlockSave.material.name);
            }
        }
        else {
            cube = _game.addBoxByName(name);
        }

        if(cube) {
            _game.addedBoxObjectArray.push(cube);
        }

        return cube;
    }
    removeBlock() {
        if( !_game.isCoordinate(_game.placeBlockSave.material.name) ) {
            if (_game.placeBlockLength) {
                // 첫 번째 물체가 제일 가까우므로 해당 물체를 고릅니다
                _game.scene.remove(_game.placeBlockSave);
                _game.addedBoxObjectArray_del(_game.placeBlockSave);

                //placeBlockSave placeBlockLength 초기화?TODO
            }
        }
    }
    isCoordinate(delname) {
        //기본평면은 삭제에서 방어.
        let delXYZ = _game.getXYZByBoxname(delname);
        return delXYZ['y'] === '0';
    }
    recordStartPosition(event) {
        // 마우스 좌표 초기화
        _game.mouse.x = event.clientX;
        _game.mouse.y = event.clientY;
        _game.mouse.moveX = 0;
        _game.mouse.moveY = 0;
    }
    recordMovement(event) {
        // 마우스 움직인 거리 측정
        _game.mouse.moveX += Math.abs(_game.mouse.x - event.clientX);
        _game.mouse.moveY += Math.abs(_game.mouse.y - event.clientY);
    }
    placeNoMovement() {

        // 마우스 움직임이 적정값 이상일 때 드래그로 처리, 적정값 이하일 때 클릭으로 처리
        if (_game.mouse.moveX < 5 && _game.mouse.moveY < 5) {
            // erase 체크되어있을 때 블록 삭제, 그렇지 않으면 블록 추가
            if(0) {
                _game.removeBlock();
                _game.clearPickPosition();
                _game.isBoxAdded = false;
                _game.prevAddBoxName = '';
            } else {
                _game.placeBlock();
            }
        }
        window.removeEventListener('pointermove', _game.recordMovement);
        window.removeEventListener('pointerup', _game.placeNoMovement);
    }
    addCameraGui() {

        const gui = new dat.GUI();
        //_game.light1.intensity = 15;
        //_game.light2.intensity = 15;
        gui.addFolder('camera');
        gui.add(_game.light1,'intensity', 0, 5);
        gui.add(_game.light2,'intensity', 0, 5);

        gui.addFolder('light position');
        gui.add(_game.light1.position,'x', -10, 10);
        gui.add(_game.light1.position,'z', -10, 10);
        gui.add(_game.light1.position,'y', -10, 10);

        gui.add(_game.light2.position,'x', -10, 10);
        gui.add(_game.light2.position,'z', -10, 10);
        gui.add(_game.light2.position,'y', -10, 10);

        gui.addFolder('arrow position');
        gui.add(_game.arrowcube.position,'x',-10,10);
        gui.add(_game.arrowcube.position,'z',-10,10);
        gui.add(_game.arrowcube.position,'y',-10,10);

        gui.addFolder('light rotation');
        gui.add(_game.arrowcube.rotation,'x',-10,10);
        gui.add(_game.arrowcube.rotation,'y',-10,10);
        gui.add(_game.arrowcube.rotation,'z',-10,10);

        gui.addFolder('camera position');
        gui.add(_game.camera.position,'x',-10,10);
        gui.add(_game.camera.position,'y',-10,10);
        gui.add(_game.camera.position,'z',-10,10);

        /*gui.add(_game.camera.position, 'x', -10,10);
        gui.add(_game.camera.position, 'y', -10,10);
        gui.add(_game.camera.position, 'z', -10,10);
        gui.add(_game.camera, 'fov', 0,180).onChange( function() {
            _game.camera.updateProjectionMatrix();
        } );*/

    }
    getCanvasRelativePosition(event) {
        const rect = _game.canvas.getBoundingClientRect();
        return {
            x: (event.clientX - rect.left) * _game.canvas.width / rect.width,
            y: (event.clientY - rect.top) * _game.canvas.height / rect.height,
        };
    }
    setPickPosition(event) {

        const pos = _game.getCanvasRelativePosition(event);
        _game.pickPosition.x = (pos.x / _game.canvas.width ) *  2 - 1;
        _game.pickPosition.y = (pos.y / _game.canvas.height) * -2 + 1;
        _game.isBoxAdded = false;
    }
    clearPickPosition() {
        _game.pickPosition.x = -100000;
        _game.pickPosition.y = -100000;
    }
    addBoxByName(boxname) {
        if(_game.prevAddBoxName === boxname) {
            //console.error('same name');
            //같은 아이디에 한번만 추가
            _game.isBoxAdded = true;
            return;
        }
        //console.log(boxname,'addbox');
        _game.prevAddBoxName = boxname;
        let xyz = _game.getXYZByBoxname(boxname);
        //console.log(xyz,'xyz');

        const scene = this.scene;

        let boxWidth = 1;
        let boxHeight = 1;
        let boxDepth = 1;
        const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);
        const loader = new THREE.TextureLoader();
        const x = xyz['x'];
        const y = parseInt(xyz['y'])+1;
        const z = xyz['z'];
        const name = 'x'+x+'y'+y+'z'+ z;
        if(y>=4) {
            console.log('y축은 3까지만 놓을수 있음');
            return;
        }

        let material = new THREE.MeshPhongMaterial({
            map: loader.load('./images/block.png'),
            name: name,
        });
        let cube = new THREE.Mesh(geometry, material);
        cube.position.x = xyz['x'];
        cube.position.z = xyz['z'];
        cube.position.y = parseInt(xyz['y']) + _game.yoffset;
        scene.add(cube);
        _game.isBoxAdded = true;

        return cube;
    }
    getXYZByBoxname(boxname) {
        const xindex = boxname.indexOf('x');
        const yindex = boxname.indexOf('y');
        const zindex = boxname.indexOf('z');

        const x = boxname.substr(xindex+1, yindex-xindex-1);
        const y = boxname.substr(yindex+1, zindex-yindex-1);
        const z = boxname.substr(zindex+1);
        return { x: x, y: y, z:z }
    }

    pointUp() {
        //점수+5점. 점수애니메이션
    }

    resizeRendererToDisplaySize(renderer) {
        const canvas = renderer.domElement;
        const width = canvas.clientWidth;
        const height = canvas.clientHeight;
        const needResize = canvas.width !== width || canvas.height !== height;
        if (needResize) {
            renderer.setSize(width, height, false);
        }
        return needResize;
    }
    render(time) {
        time *= 0.001;
        let renderer = _game.renderer;
        let scene = _game.scene;
        let camera = _game.camera;




        if (_game.resizeRendererToDisplaySize(renderer)) {
            const canvas = renderer.domElement;
            camera.aspect = canvas.clientWidth / canvas.clientHeight;
            camera.updateProjectionMatrix();
        }
        _game.pickHelper.pick(_game.pickPosition, scene, camera, time);

        renderer.render(scene, camera);

        requestAnimationFrame(_game.render);
    }





    /*
    * 게임오버
    */
    gameover() {
        console.log('게임오버');
        _game.isGameOver = 1;
        gameover();
    }


    remainTimeDraw() {
        //남은시간 그리기
    }
    /**
     * 캔버스 사이즈 자동결정
     */
    adjustCanvasSize() {
        let sizeObject = this.findSize();
        let $canvas = $('#'+this.canvasid);
        let $maps = $('#cmaps');
        if($canvas && sizeObject && sizeObject['width'] && sizeObject['height']) {
            $canvas.css('width', sizeObject['width']+'px').css('height', sizeObject['height']+'px');
            $canvas.attr('width', sizeObject['width']+'px').attr('height', sizeObject['height']+'px');
            if(_game.canvas) {
                _game.canvas.set('width', sizeObject['width']);
                _game.canvas.set('height', sizeObject['height']);
            }
            let $canvasparent = $canvas.parent();
            if($canvasparent) {
                $canvasparent.css('width', sizeObject['width']+'px').css('height', sizeObject['height']+'px');
            }
            if($maps.length) {
                $maps.css('width', sizeObject['width']+'px').css('height','250px');
                $maps.attr('width', sizeObject['width']+'px').attr('height','250px');
            }

            this.width = sizeObject['width'];
            this.height = sizeObject['height'];
        }
    }
    findSize() {
        //PC의경우 width아주약간넘어감,height약간작음
        //Mobile Iphone width,height정확하게 맞음
        //Mobile Android TODO
        const mixvalue = 1;
        const mixvalueh = 1;
        let width = Math.min(screen.width * mixvalue, screen.availWidth * mixvalue , $('body').outerWidth() * mixvalue, document.body.offsetWidth * mixvalue, document.body.clientWidth * mixvalue, window.outerWidth * mixvalue, window.innerWidth * mixvalue ) ;
        let height = Math.min(screen.height * mixvalueh, screen.availHeight * mixvalueh, window.outerHeight * mixvalueh, window.innerHeight * mixvalueh);

        /*console.log(screen.width,'screen.width');
        console.log(screen.availWidth,'screen.availWidth');
        console.log($('body').outerWidth(),'$body outerWidth');
        console.log(document.body.offsetWidth,'document.body.offsetWidth');
        console.log(document.body.clientWidth,'document.body.clientWidth');
        console.log(window.outerWidth,'window.outerWidth');
        console.log(window.innerWidth,'window.innerWidth');*/

        /*
        console.log(screen.height,'screen.height');
        console.log(screen.availHeight,'screen.availHeight');
        console.log($('body').outerHeight(),'$body outerheight');
        console.log(document.body.offsetHeight,'document.body.offsetHeight');
        console.log(document.body.clientHeight,'document.body.clientHeight');
        console.log(window.outerHeight,'window.outerHeight');
        console.log(window.innerHeight,'window.innerHeight');
        console.log(width+':'+height,'width:height');
        */
        let $topnav = $('#topnav');
        let topnavheight = $topnav.css('height');
        if(topnavheight) {
            topnavheight = topnavheight.replace('px','');
        }
        if($topnav.css('display') === 'none') {
            topnavheight = 0;
        }

        let cparentmargintop = $('#cparent').css('margin-top').replace('px','');
        height -= topnavheight;
        height -= cparentmargintop;

        //If PC Screen
        let widthmax = 320*1.5, heightmax = 480*1.5;
        if(width > widthmax || height > heightmax) {
            width = widthmax;
            height = heightmax;
        }


        let returnObject = {};
        returnObject['width'] = width;
        returnObject['height'] = height;
        return returnObject;
        //clientwidth,clientheight, outerWidth, outerHeight, 등등 사용.
    }
    makeSolution() {
        //정답 불러오기
        let solution = [];
        for(let x=0; x<4; x++) {
            solution[x] = [];
            for(let z=0; z<4; z++) {
                solution[x][z] = [];
                for(let y=0; y<4; y++) {
                    solution[x][z][y] = 2;
                }
            }
        }
        let goodseearray;
        let solutionindex;

        if(_game.blockcount === 3) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*4)+1; //1~4
            switch(solutionindex) {
                case 1:
                    goodseearray[0] =
                        [0,1,0,0,
                            1,0,0,0,
                            0,0,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [1,0,0,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 2:
                    goodseearray[0] =
                        [0,0,1,1,
                            0,0,1,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,1,0,0,
                            0,1,1,0,
                            0,0,0,0];
                    break;
                case 4:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,0,1,0,
                            0,1,0,0,
                            0,0,1,0];
                    break;
                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');

        }
        else if(_game.blockcount === 4) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*4)+1; //1~4
            switch(solutionindex) {
                case 1:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,1,0,1,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,0,1,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 2:
                    goodseearray[0] =
                        [0,0,1,1,
                            0,1,0,0,
                            1,0,0,0,
                            0,0,0,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,1,0,
                            0,1,0,0,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,0,0,
                            0,1,0,0,
                            0,0,0,0];
                    break;
                case 4:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,0,1,0,
                            0,1,0,0,
                            0,0,0,1];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,0,0,
                            0,1,0,0,
                            0,0,0,0];
                    break;
                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');
        }
        else if(_game.blockcount === 5) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*3)+1; //1~4
            switch(solutionindex) {
                case 1:

                    goodseearray[0] =
                        [0,0,1,1,
                            0,1,0,0,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,1,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 2:
                    goodseearray[0] =
                        [1,0,1,1,
                            0,0,0,0,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [0,0,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            1,0,0,0]
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,1,0,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[2] =
                        [0,0,0,0,
                            0,0,1,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;

                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');
        }
        else if(_game.blockcount === 6) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*3)+1; //1~4
            switch(solutionindex) {
                case 1:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,1,1,1,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,0,1,
                            0,0,0,0,
                            0,0,0,0];
                    goodseearray[2] =
                        [0,0,0,0,
                            0,0,0,1,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 2:
                    goodseearray[0] =
                        [0,0,0,1,
                            0,1,0,0,
                            0,1,0,0,
                            1,0,0,0];
                    goodseearray[1] =
                        [0,0,1,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,1,0,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,1,1,
                            0,0,0,0,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,1,0,
                            0,0,1,0,
                            0,0,0,0];
                    goodseearray[2] =
                        [0,0,0,0,
                            0,0,0,1,
                            0,0,0,0,
                            0,0,0,0];
                    break;

                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');
        }
        else if(_game.blockcount === 7) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*3)+1; //1~4
            switch(solutionindex) {
                case 1:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,0,0,0,
                            0,0,1,0,
                            1,0,0,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,1,1,
                            1,0,1,0,
                            1,0,0,0];
                    break;
                case 2:
                    goodseearray[0] =
                        [0,1,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            1,0,0,0];
                    goodseearray[1] =
                        [0,0,1,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,1,0,0];
                    goodseearray[2] =
                        [0,0,1,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,0,1,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            1,0,0,0];
                    goodseearray[1] =
                        [0,0,1,0,
                            0,0,1,0,
                            1,1,0,0,
                            0,0,0,0];
                    goodseearray[2] =
                        [0,0,0,0,
                            0,0,0,0,
                            0,0,1,0,
                            0,0,0,0];
                    break;

                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');
        }
        else if(_game.blockcount === 8) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*3)+1; //1~4
            switch(solutionindex) {
                case 1:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,1,1,1,
                            0,1,0,0,
                            0,1,0,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,0,1,
                            0,0,0,0,
                            0,0,0,0];
                    goodseearray[2] =
                        [0,0,0,0,
                            0,0,0,1,
                            0,0,0,1,
                            0,0,0,0];
                    break;
                case 2:
                    goodseearray[0] =
                        [0,1,0,0,
                            1,1,0,0,
                            0,0,0,0,
                            0,1,1,0];
                    goodseearray[1] =
                        [0,0,0,0,
                            1,0,0,0,
                            0,0,0,0,
                            0,0,1,0];
                    goodseearray[2] =
                        [0,0,0,0,
                            1,0,0,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,1,1,
                            1,1,1,0,
                            0,1,0,0,
                            0,0,0,0];
                    goodseearray[1] =
                        [0,0,0,1,
                            1,0,0,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;

                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');
        }
        else if(_game.blockcount === 9) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*3)+1; //1~4
            switch(solutionindex) {
                case 1:
                    goodseearray[0] =
                        [1,1,1,1,
                            0,1,0,0,
                            0,0,0,0,
                            0,0,0,0]
                    goodseearray[1] =
                        [1,0,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            0,0,0,0]
                    goodseearray[2] =
                        [1,1,0,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;
                case 2:
                    goodseearray[0] =
                        [0,0,0,1,
                            0,1,1,0,
                            0,1,0,1,
                            1,0,0,1]
                    goodseearray[1] =
                        [0,0,0,1,
                            0,0,0,0,
                            0,1,0,0,
                            0,0,0,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,0,0,
                            0,0,1,1,
                            1,0,1,0,
                            0,1,0,0]
                    goodseearray[1] =
                        [0,0,0,0,
                            0,0,1,0,
                            1,0,1,0,
                            0,0,0,0];
                    goodseearray[2] =
                        [0,0,1,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,0,0,0];
                    break;

                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');
        }
        else if(_game.blockcount === 10) {
            goodseearray = [];
            solutionindex = Math.floor(Math.random()*3)+1; //1~4
            switch(solutionindex) {
                case 1:
                    goodseearray[0] =
                        [0,0,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            1,0,0,0]
                    goodseearray[1] =
                        [0,0,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            1,0,0,0];
                    goodseearray[2] =
                        [0,0,0,1,
                            0,0,0,1,
                            0,0,0,1,
                            0,1,1,1];
                    break;
                case 2:
                    goodseearray[0] =
                        [0,1,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            1,0,1,0]
                    goodseearray[1] =
                        [0,1,0,1,
                            0,0,0,0,
                            0,0,0,0,
                            1,0,1,0];
                    goodseearray[2] =
                        [0,0,1,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,1,0,0];
                    break;
                case 3:
                    goodseearray[0] =
                        [0,0,0,0,
                            1,1,1,1,
                            0,0,1,0,
                            1,1,1,0]
                    goodseearray[1] =
                        [0,0,0,0,
                            1,0,0,0,
                            0,0,0,0,
                            0,0,1,0];
                    break;

                default:
                    alert('스테이지 오류 발생(11)');
                    break;
            }
            solution = _game.arrayToSolution(goodseearray);
            _game.goodseearray = goodseearray;
            _game.solution = solution;
            console.log(goodseearray,'goodseearray');
            console.log(solution,'solution');
        }

        //중복이면 다시실행
        let currentquestiontext = 'blockcount'+_game.blockcount+'solutionindex:'+solutionindex;
        console.log(currentquestiontext)
        for(let i=0; i < _game.viewedQuestionArray.length; i++) {
            if( _game.viewedQuestionArray[i] === currentquestiontext) {
                _game.samecount++;
                if(_game.samecount > 10) {
                    break;
                }
                console.log('getstage 재시도 카운트'+ _game.samecount);
                _game.solution = '';
                _game.getStage();
                return;
            }
        }

        _game.viewedQuestionArray.push(currentquestiontext);
        _game.samecount = 0;
    }
    /*
    * 텍스트 만들기 유틸
    *  text 쓸 글자
    *  option {left: 45, top: 120, fontFamily: 'Comic Sans', fontSize: 10, fill:'#FFFFFF', opacity:0.5}
    *  etc  {align:'xcenter'}  추가 모드들
    */
    textadd(text,options, etc) {
        if(options === undefined)
            options = {};
        var temptext = new fabric.Text(text,options);
        temptext.hasBorders = false;
        temptext.hasControls = true;
        temptext.lockRotation = true;
        temptext.lockMovementX = true;
        temptext.lockMovementY = true;
        temptext.selection = false;
        temptext.selectable = false;

        let textwidth;
        let textpositionx;
        //console.log(etc,'etc');
        if(etc) {
            if(etc.align === 'xcenter') {
                textwidth = temptext.aCoords.tr.x - temptext.aCoords.tl.x;
                //320x480
                textpositionx = (((320 * this.widthweight) / 2) - (textwidth / 2)) ;
                temptext.set({
                    left: textpositionx,
                });
                console.log('setxcenter:'+textpositionx);
            }
            if(etc.align === 'xcenter1/2') {
                //가로 길이의 1/2지점에서 센터를 잡음. 가로 320일때 0~160사이의 센터.
                textwidth = temptext.aCoords.tr.x - temptext.aCoords.tl.x;
                textpositionx = (((320 * this.widthweight) / 2 / 2) - (textwidth / 2)) ;
                temptext.set({
                    left: textpositionx,
                });
                console.log('setxcenter:'+textpositionx);
            }
            if(etc.align === 'xcenter2/2') {
                //가로 길이의 2/2지점에서 센터를 잡음. 가로 320일때 160~320사이의 센터.
                textwidth = temptext.aCoords.tr.x - temptext.aCoords.tl.x;
                textpositionx = (((320 * this.widthweight) / 2 / 2) + ((320 * this.widthweight) / 2) - (textwidth / 2)) ;
                temptext.set({
                    left: textpositionx,
                });
                console.log('setxcenter:'+textpositionx);
            }

            if(etc.bottomalignTo) {
                console.log(etc.bottomalignTo);
                //top:relaxed.top + relaxed.height,
                temptext.set({
                    //top: etc.bottomalignTo.top + etc.bottomalignTo.height - temptext.height ,
                    top: etc.bottomalignTo.top + etc.bottomalignTo.height - temptext.height ,
                });
            }
        }
        return temptext;
    }
    /*
    * 틀린이미지 보여주기
    */
    viewInCollectImage() {
        //배경추가 collectimage incollectimage  web: left130 top190  mobile: left110 top170
        var top = '';
        var left = '';
        var image = '';
        if(_game.browser === 'web') {
            top = 30;
            left = 130;
            image = './images/fail-r.png';
        }
        else if(_game.browser === 'mobile') {
            top = 30;
            left = 130;
            image = './images/fail-r-m.png';
        }

        if(!_game.incollectimage) {

            fabric.Image.fromURL(image, function(img) {
                _game.incollectimage = img.set({
                    left: left*_game.widthweight
                    , top: top*_game.heightweight

                });
                _game.canvas_fabric.add(_game.incollectimage);
                setTimeout( function() {
                    _game.canvas_fabric.remove(_game.incollectimage);
                    _game.incollectimage = '';
                } , 200);
            });
        }
    }
    /*
    * 사운드를 cache한다.
    */
    soundPreload() {
        createjs.Sound.registerSound("./sound/correct-1.mp3", 'soundCorrect');
        createjs.Sound.registerSound("./sound/incorrect-1.mp3", 'soundInCorrect');
    }
    playSoundCorrect() {
        if(!_game.silence) {
            createjs.Sound.play('soundCorrect');
        }
    }
    playSoundInCorrect() {
        if(!_game.silence) {
            createjs.Sound.play('soundInCorrect');
        }
    }
    /*
    * 맞은이미지 보여주기.
    */
    viewCollectImage() {
        //배경추가 collectimage incollectimage  web: left130 top190  mobile: left110 top170
        var top = '';
        var left = '';
        var image = '';
        if(_game.browser === 'web') {
            top = 30;
            left = 130;
            image = './images/pass-g.png';
        }
        else if(_game.browser === 'mobile') {
            top = 30;
            left = 130;
            image = './images/pass-g-m.png';
        }

        if(!_game.collectimage) {
            fabric.Image.fromURL(image, function(img) {
                _game.collectimage = img.set({
                    left: left*_game.widthweight
                    , top: top*_game.heightweight
                });
                _game.canvas_fabric.add(_game.collectimage);
                setTimeout( function() {
                    _game.canvas_fabric.remove(_game.collectimage);
                    _game.collectimage = '';
                } , 200);
            });
        }
    }
    scoresave() {

    }
    /*
    * log 게임시작시간, 최대맞출수있는개수, 맞춘개수, n, 점수, 유저 저장. log ()
    */
    log2(timingtext) {
        var $deferred = $.Deferred();

        var dataarray = {};

        console.log(' 로그남기기 logseq:'+_game.logseq+'     타이밍:'+timingtext);

        dataarray = {userseq:_game.userseq, timingtext:timingtext,maxpointcount:_game.maxpointcount, mypointcount:_game.mypointcount, n:_game.n, point:_game.mypoint
            , mode:_game.mode, logseq: _game.logseq,  browser:_game.browser};
        var url = '/game/block/ajax/write_block_log.ajax.php';
        $.ajax({
            type:'POST',
            url:url,
            data:dataarray,
            dataType:'html',
            cache:false,
            success:function(x){
                console.log(x,'/game/block/ajax/write_block_log.ajax.php');
                if(x==='' || x===undefined || x==='null' || x===null)
                    return;
                var xp = jQuery.parseJSON(x);
                if(xp['result'] === '0'){
                    alert(xp['resulttext']);
                    return $deferred.reject();
                }
                else if(xp['result'] === '1'){

                    switch(timingtext) {
                        case 'start':
                            //게임시작
                            return $deferred.resolve(xp['logseq']);

                        case 'gaming':
                            //게임중(점수가 올라가거나 내려감)
                            return $deferred.resolve(xp['logseq']);

                        case 'end':
                            //게임종료(타이머가 끝남)
                            return $deferred.resolve(xp['logseq']);

                        default:
                            return $deferred.reject();
                    }

                }
            },
            error:function(xhr,textStatus,errorThrown){
                if ( typeof minmail !== "undefined" ) {
                    minmail(url,dataarray,xhr.status,xhr.statusText,xhr.responseText,textStatus,errorThrown);
                }
                return $deferred.reject();
            }
        });
        return $deferred.promise();

    }
}
